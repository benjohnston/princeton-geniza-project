<TEI xmlns="http://www.tei-c.org/ns/1.0">
    <teiHeader>
        <fileDesc>
            <titleStmt>
                <title>Bodl MS Heb D 66 121v </title>
                <editor/>
            </titleStmt>
            <publicationStmt>
                
            <distributor><idno type="PGP">5396</idno></distributor></publicationStmt>
            <sourceDesc>
                <msDesc>
                    <msIdentifier>
                        <repository>Library</repository>
                        <msName/>
                    </msIdentifier>
                    <msContents>
                        <p>Bodl MS Heb d 66 121v<lb/>LEGAL DOCUMENT <lb/>{<lb/>  Ed. Amir Ashur, "Engagement and Betrothal Documents from the Cairo Geniza" (Hebrew) (PhD dissertation, Tel Aviv University, 2006), Doc. A-40, pp. 272-4; also S. Assaf, "Ancient Legal Deeds," p. 213. <lb/>  This interesting document, from December 1027, is probably a copy of a legal case that was inserted into a "legal file" that was kept in the court. Documents describing unique or interesting legal cases were occasionally copied in order to serve the clerks of the court as reference (Goitein also suggests that the document might have been copied to serve as an accompaniment to a legal discussion. However, Ashur disagrees as the copy was made eight years after the actual case and there is no evidence to support Goitein's claim; see Goitein, Med. Soc. 3:88 and 3:444, no. 69). We can tell that this is a copy of the original document as the names of the litigants are not mentioned and the document is not signed. On the other hand, the detailed description of the entire process, the dates, the exact monetary sums, the number of rings and their type, all testify that this was a real case. Goitein suggests that the document represent an agreement made by a poor couple. <lb/>  This is the only document known to Ashur in which the word "betrothals" (Shiddukhim) means both "marriage gift" and "consecration money" ("matnat nissū’īn" and "kesef qiddūshīn").<lb/>The body of the document contains two dates. The first, in the beginning of the document, refers to the date in which the groom proclaimed, in front of witnesses, his desire to marry the bride and gave them the three rings. The second date, 17 days later, is probably the date in which the engagement contract was delivered to the bride's representative (Assaf presented a different view – the second date is the date in which the matter was put on paper while the first date signify the time of the engagement which took place on a single day. However, Ashur objects as it was customary to clearly mention the fact that a document was signed at a later date). <lb/>  The Muslim year is mentioned in the beginning of the document (year 428 is equivalent to 1036 C.E) eight years after the events described in the document. Assaf, who was the first to publish the document, claimed that the date and the title "what took place" do not belong to the rest of the document. However, Ashur claims that this date is the date in which the document was copied to the legal documents file in court and in the same occasion the title was written, since the recto also has the year recorded at the header (Assaf missed the word "year" in the header of the recto. <lb/>  The process of preparing the betrothal is described in great detail which is unmatched by other Geniza documents. The groom addresses the witnesses, who act as arrangers of the betrothal and were probably sent by the court, and declares his desire to "engage and betroth" the bride. After this declaration, he hands over three betrothals ("shiddūkhīm") – rings that are supposed to later serve as betrothal objects. The witnesses ask him about the sum of the marriage gift (mōhār). The groom responds that when he will have the means to do so, he will hand it over to the bride or to her representative personally. From checking dozens of engagement agreements, it is clear that the sum of the marriage gift was one of the first things agreed upon by the bride and groom. It appears that the witnesses wanted to know the sum of the Mohar so they can report it to the bride or her representative, and obtain her approval to the betrothal. In the next stage, the witnesses arrive to the bride and make s<lb/>ure she appointed her representative. Then they handed over the rings to the bride's representative as an act of betrothal.
}</p>
                        <p/>
                    </msContents>
                </msDesc>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <textClass>
                <keywords>
                    <term/>
                </keywords>
            </textClass>
        </profileDesc>
        <revisionDesc>
            <change when="2013-12-24" who="#BJ">TEI created</change>
        </revisionDesc>
    </teiHeader>
    <text xml:space="preserve">
        <body>
            <div>
                <l n="1">	מעשה         סנה תמאן עשרים וארבע מאיה</l>
                <l n="2">מעשה שהיה בפנינו אנו העדים החתומים למטה בכתב הזה בחמישי</l>
                <l n="3">בשבא שהוא עשרים וששה ימים לירח טבת שנת אשלט לשטרות כי</l>
                <l n="3">מ פלני בר פלני אמר לפנינו שאני חפץ לארס ולקדש את פלנית בר מ</l>
                <l n="4">פלני בקידושים גמורים והנה השידוכים שאתנם לה והוציא לנו &lt;&lt;שלש&gt;&gt;</l>
                <l n="5">שלשה שידוכים חותמות אחד  של זהב משובך והשנים &lt;&lt;משבך&gt;&gt;</l>
                <l n="6">של כסף ואמרנו לו אילו השדוכים שתתן לה וכמה יהיה המֹהר</l>
                <l n="7">השיב לנו ואמר עשרים זהובים טובים עשרה מהם מקדמים</l>
                <l n="8">ועשרה מאוחרים אמרנו ואיכן הזהובים המוקדמים</l>
                <l n="9">אמר לנו אינם עכשיו עמי אלא אני אתנם לה או לאפטרופוס</l>
                <l n="10">&lt;&lt;שלה&gt;&gt; שלה בעת שיזמנם הקבה ועמדנו לפלנית בת מ פלני</l>
                <l n="11">וקנינו מידה מאחר שהשתמודענה  בשני עדים ברורים </l>
                <l n="12">שהרשת את פלני בר פלני אפטרופוס שלה ושבנו וארסנו</l>
                <l n="13">את פלני בר פלני לפלונית בר פלני בקידושים גמורים ונתננו השלשה</l>
                <l n="14">השידוכים לפלני בר פלני האפטרופוס וכמה שהיה לפנינו </l>
                <l n="15">כתבנו וחתמנו להיות לזכות ולראיה לאחר היום נכתב בשני</l>
                <l n="16">בשבת שהוא חמשה עשר ימים לחודש שבט שנת אלף</l>
                <l n="17">ושלש מאות ושלשים ותשעה למנין שטרות שאנו רגילין </l>
                <l n="18">למנות בו בפסטאט מצרים שעל נילס הנהר מושבה</l>
                <l n="19">	שריר וקים</l>
            </div>
        </body>
    </text>
</TEI>
