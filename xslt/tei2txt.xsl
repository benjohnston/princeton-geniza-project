<xsl:stylesheet xmlns="http://www.w3.org/1999/xhtml" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:tei="http://www.tei-c.org/ns/1.0" version="1.0">

   <xsl:output method="html" doctype-system="http://www.w3.org/TR/html4/strict.dtd" doctype-public="-//W3C//DTD HTML 4.01//EN" indent="yes"/>
   <xsl:preserve-space elements="l"/>
   <xsl:template match="/">
     <xsl:apply-templates/>
   </xsl:template>

  <xsl:template match="tei:teiHeader"/>

   <xsl:template match="tei:text">
     <div class="transcription">
     <xsl:apply-templates/>
     </div>
   </xsl:template>

  <xsl:template match="tei:body">
    <xsl:apply-templates/>
  </xsl:template>
  
  <xsl:template match="//tei:l[not(@rend='col')]">
            <xsl:if test="@n">
                    <xsl:value-of select="@n"/>&#160;
            </xsl:if>
            <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="//tei:p">
    <xsl:apply-templates/>
  </xsl:template>



  <xsl:template match="//tei:desc">
            <xsl:apply-templates/>
  </xsl:template>
  
  <xsl:template match="//tei:l[@rend='col']"/>

  <xsl:template match="//tei:label">
            <xsl:apply-templates/>
  </xsl:template>
  
  <xsl:template match="tei:span">
            <xsl:apply-templates/>
  </xsl:template>
  
  <xsl:template match="tei:hi[@rend='superscript']">
     <sup>
     <xsl:apply-templates/>
     </sup>
   </xsl:template>

  <xsl:template match="//tei:note">
    <div class="note">
            <xsl:apply-templates/>
        </div>
  </xsl:template>


  <xsl:template match="//tei:incident">
    <h1 style="font-size:10em;color:red;">!</h1>
  </xsl:template>



<xsl:template match="//tei:cb">
    <div class="hebrew-column">
      <xsl:variable name="numTextNodes" select="count(following::tei:l[@rend='col']) - count(following::tei:cb/following::tei:l[@rend='col'])"/>
        <xsl:for-each select="following::tei:l[position() &lt;= $numTextNodes]">
          
      <div class="hebrew">
                    <span class="number">
                        <xsl:value-of select="@n"/>
                    </span>
                    <xsl:value-of select="."/>
                </div>
          <!--<xsl:if test="not(following-sibling::cb)"><div style='clear:both'/></xsl:if>
          <xsl:value-of select="following-sibling::text()"/>-->
        </xsl:for-each>
    </div>
</xsl:template>
</xsl:stylesheet>
