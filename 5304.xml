<TEI xmlns="http://www.tei-c.org/ns/1.0">
    <teiHeader>
        <fileDesc>
            <titleStmt>
                <title>TS 8 J 21 12</title>
                <editor/>
            </titleStmt>
            <publicationStmt>
                
            <distributor><idno type="PGP">5304</idno></distributor></publicationStmt>
            <sourceDesc>
                <msDesc>
                    <msIdentifier>
                        <repository>Library</repository>
                        <msName/>
                    </msIdentifier>
                    <msContents>
                        <p>TS 8 J 21.12<lb/>
LETTER<lb/>
{<lb/>
Ed. Miriam Frenkel, "The Compassionate and Benevolent: The Leading Elite in the Jewish Community of Alexandria in the Middle Ages" (Hebrew) Jerusalem: Yad Ben Zvi, 2006. Doc. #45, pp. 437-441. Also ed. Friedman, "Ze‛aqat Shever," Pe‛amim 78 (1999): 128-147. Friedman’s notes appear in brackets.<lb/>
A letter from the Alexandrian community to Samuel ha-Levi, "The prince of Levites, the glory of doctors" b. Solomon, requesting his help in countering a decree that forbade saying piyoutim (Heb. sacred poetry) during prayer.<lb/>
Goitein argued that the letter was written from a country town in Egypt while Friedman suggested that the letter originated from the Jerusalemite Synagogue in Fustat and that the decree forbidding piyoutim was part of the Hasidic reforms of prayer which Abraham Maimonides tried to implement.<lb/>
In Frenkel’s opinion, the letter was sent from Alexandria. There are a few indications for this in the letter: The mentioning of "the evils of gentiles" (recto - line 17) fits well with Alexandria which was known for its fanatic atmosphere and for its not uncommon anti-Jewish acts of violence; see Goitein, Med. Soc. 2:278-281. The same is true for the mention of an epidemic lung diseases common to Alexandria (recto – right margin line 1 and the note to this line). Furthermore, Alexandria was known for its characteristic prayer customs, to which the Alexandrian Jewish community adhered with zeal; see for example the responsa sent by Saadya b. Berakhot ha-Melammed from Alexandria to Maimonides (Maimonides, Responsa, vol. 1, #113, p. 196; vol. 1, #118, p. 202; vol. 2, #163, pp. 311-312; vol. 2, #259, p. 485). Friedman’s argument that the decree forbidding piyoutim originated from Abraham Maimonides fits well with the argument that the letter was sent from Alexandria. A group of pious followers (Heb. Hassidim) of Abraham Maimonides settled in Alexandria and strove to implement in the wider community the principals of their creed. The reference to these pious followers as "judges, cantors and managers of pious foundations" (Verso, line 3) agrees with other hints about the followers of Abraham Maimonides as enjoying material benefits and neglecting their spiritual mission. On the active followers of Abraham Maimonides in Alexandria see Goitein, "Our Rabbi Abraham," Trabitz 33 (1964): 181-197.<lb/><lb/>
(Information from Frenkel).<lb/>
}</p>
                        <p/>
                    </msContents>
                </msDesc>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <textClass>
                <keywords>
                    <term/>
                </keywords>
            </textClass>
        </profileDesc>
        <revisionDesc>
            <change when="2013-12-24" who="#BJ">TEI created</change>
        </revisionDesc>
    </teiHeader>
    <text xml:space="preserve">
        <body>
            <div>
                <desc rend="center strong">תעודה מספר 45</desc>
                <desc rend="center strong">TS 8 J 21. 12</desc>
                <desc rend="strong">מכתב מאנשי קהילת אלכסנדריה אל שמואל הלוי 'שר הלויים פאר הרופאים' בר שלמה, בבקשה לעזור להם בביטול הגזרה האוסרת על אמירת פיוטים בתפילה.</desc>
                <desc>גויטיין סבר כי המכתב נכתב בעיר שדה במצרים; ואילו פרידמן העלה את הסברה כי מקורו של המכתב בבית הכנסת הירושלמי בפסטאט, וכי הגזרה האוסרת על אמירת פיוטים היא חלק מתיקוני התפילה החסידיים שניסה להחיל ר' אברהם בן הרמב"ם.</desc>
                <desc>לדעתי נשלח המכתב מקהילת אלכסנדריה. יש לכך מספר תימוכין בגוף המכתב: הזכרת 'רשעת הגויים'" (ע"א, שורה 17) מחזקת את הסברה שהמדובר באלכסנדריה, שנודעה באווירה הקנאית ובהתפרצויות האנטי-יהודיות שאירעו בה חדשות לבקרים. ראו על כך אצל גויטיין חברה, ב, עמ' 281-278. הזכרת מחלות הריאה האפידמיות האופייניות לאלכסנדריה מרמזת אף היא על כיוון זה (ראו להלן, ע"א בשוליים הימניים, שורה 1 והערה לשורה זו). יתרה מזאת, ידוע לנו על מנהגי תפילה מיוחדים שנהגו באלכסנדריה ואשר אנשיה דבקו בהם בקנאות. ראו למשל את השאלות ששלח סעדיה בר ברכות המלמד מאלכסנדריה אל הרמב"ם (תשובות הרמב"ם, א, קיג, עמ' 196; שם, קיח, עמ' 202; ב, קסג, עמ' 312-311; שם, רנט, עמ' 485). מיקום המסמך באלכסנדריה מתיישב יפה גם עם הנחתו של פרידמן כי הגזרה על ביטול הפיוטים באה מידי הראב"ם, שהרי ידוע לנו על קבוצת חסידים מתלמידיו של הראב"ם שישבה באלכסנדריה וניסתה להחיל על קהילתה את עיקרי אמונתה. היותם של חסידים אלו 'דיינים וחזנים ומנהלי בתי הקדש' (ע"ב, שורה 3) עולה בקנה אחד עם הרמיזות בדבר התלמידים מקרב חסידי הראב"ם שנהנו מטובות הנאה חומריות והזניחו את שליחותם הרוחנית. על קבוצת תלמידי הראב"ם ופעילותה באלכסנדריה ראו גויטין, רבנו אברהם.</desc>
                <desc rend="strong">תאריך משוער: סביבות שנת 1211, זמן המאבק סביב תיקוני התפילה.</desc>
                <desc rend="strong">פורסם: פרידמן, זעקת שבר. הערותיו צוינו [פרידמן].</desc>
                <desc rend="strong">נזכר: גויטיין, חברה, ב, עמ' 160; פנטון, תפילה, עמ' 18.</desc>
                <label>Recto</label>
                <l n="">צד ימין</l>
                <l n="1-3 "> וללוי אמר [  ]ב' שומ[ר הבטחתו ] כי אורך ימים</l>
                <l n="">צד שמאל</l>
                <l n="1-3"> [     ]אלי יסתגיב פי אלחצרה אלסאמיה [אל]כרימה הדרת כג'ק מ'ר' ור' שמואל השר היקר שר הלוים פאר הרופאים ב'ר' כ'ג'ק'</l>
                <l n="4 ">מר' ור' שלמה השר היקר פאר הלוים נ'ע' צאלח אלתי קד אשתהר</l>
                <l n="5 ">באלפצל אמרהא / וכתר באלאחסאן כירהא/ רצ'י אללה ען כרים סלפהא / ואבק[אהא]</l>
                <l n="6 ">לקאצדהא / ובלג אלאמאל בחסן אראהא / ולא אכלא אחבאהא מן רויתהא /</l>
                <l n="7 ">פאן מנט'רהא שפא ללאמראץ/ ובמלאחצ'תהא תנאל אלאגראץ' / ובהמתהא</l>
                <l n="8 ">תקוא אלעזאים אלסלפיה / ובעט'ים נכותהא תתבת אלאמור אלשרעיה / אלתי</l>
                <l n="9 ">יקאל אנהא קד הוות / ואן רסומהא אמחת / ואן ואצ'עיהא גלטו / ואן אלאבא אכטו</l>
                <l n="10 ">ולבטו / ומא ערף אלקאיל פצ'ל אלרחמן / ומא אדדכר פי אלזמאן / מן אהל</l>
                <l n="11 ">אלביותאת / ודו אלמרואת / אלדי יגדד בנכותהם מא קיל הוא / ויעיד מא קיל</l>
                <l n="12 ">אמתחא / ויצח קול אלואצ'עין רחמה אללה / עליהם אגמעין / ויצדק קול</l>
                <l n="13 ">אלאבא / במן בה לבא / ותסתקים אלאמור בעד פסאדהא / בחיאתך וישתהר</l>
                <l n="14 ">רשאדהא גיר כפי ען אלמולא מא ג'רי פי אמר אלתפילות ואלמנהגות מן אלתי</l>
                <l n="15 ">מא קנעו לנא באלגלות ואלעניות ואלשפלות בין אומות העולם וכנא נתסנא</l>
                <l n="16 ">באגתמאענא פי בתי כניסיות ללקדושה וקדיש ומרוב העונות חית כפינא</l>
                <l n="17 ">שר אלגויים בעת בשר מן יחב אלריאסה ואלנפאסה וכאן אול אלזמאן חב 	אלדי[ן]</l>
                <l n="18 ">באלתכתיר פי מחאסן אלמד[חא ]ת פכאנת אלאעיאד באלקרובאת ואלסבות</l>
                <l n="19 ">באלמעריבאת ואלקאויל פי אלצלאת והדא פאמר משהור פי סאיר</l>
                <l n="20 ">אלבלאד ובה בידינא פתאוי עלמאנא באנה גאיז וליס פסאד ואהל</l>
                <l n="21 ">אליום יקצדו נקץ מא רתבוה אלאואיל ותרדיל אלצדיקים ארבאב ואלפצ'א[יל]</l>
                <l n="22 ">ויטאלבונא באלדכול פי אראיהם וקבול פתיאהם ותרך מעתקדנא ח[תי]</l>
                <l n="23 ">יכון אשד עלינא פי תכדיב נפוסנא ותמחיל אבאינא ונקול אך שקר</l>
                <l n="24 ">נחלו אבותינו הבל ואין בם מועיל ונחן מעשר אלאסראליה ואלאמה</l>
                <l n="25 ">אלמוסויה ומא נכרג ען קול אלולי אשר שמענו ונדעם ואבותינו ספרו</l>
                <l n="26 ">לנו פועל פעלתה בימיהם בימי קדם ולא נכלי קול אלתורה לא תסיג גבול</l>
                <l n="27 ">רעך אשר גבלו ראשונים וקולה ארור מסיג גבול רעהו וקול אלסיד</l>
                <l n="28 ">שלמה לא תסיג גבול עולם וחמל מצאררינא חב אלריאסה ואלאמר</l>
                <l n="">אלי אן תרכו קול אלנבי ע'א' ושונה מפריד אלוף וקולה ירא את י' בני</l>
                <label>Margin</label>
                <l n="1 ">ומלך עם שונים אל תתתערב פאן ראיתונא מעשר ישראל אן קד תמם עלינא וקתלנא בכתרה אלבראסים</l>
                <l n="2 ">עלינא פאעלמוא אן עלי מקאל אבאינא ואגדנא ואנביאנא קתלנא ואן אסתר מע מלכהא וסעאדתהא</l>
                <label>ע"ב</label>
                <l n="1 ">תאתרת לנכוה אלסיד מרדכי ובדלת נפסהא מרוה כמא קאל אלנץ</l>
                <l n="2 ">אבוא אל ה מלך אשר לא כדת ומעלום אן האולאי מתקיין באלמאל וכתרה</l>
                <l n="3 ">אלרג'אל לעלה לא ללה פאן קד צארו כלהם דיאנין וחזאנין ומתוליין אלרבע</l>
                <l n="4 ">ומא יכפי ען אלמולא פקרנא ואגאחתנא ולא יטיב לנא תכליה מא באידינא</l>
                <l n="5 ">וגרמאנא מא בידהם שי פאדא תרכו ראיהם מא [  ] שי וקד אכדנא</l>
                <l n="6 ">בענאיה אלבארי ת'ע' פתאוי אלפקהא באן לא יגיר עלינא שי ונחן פי</l>
                <l n="7 ">אשד שדה תכון פאללה ת'ע' יפרגהא וקד אסתנגזנא תוקיע אלסלטאן</l>
                <l n="8 ">ומא קבלוהם מנא וקד ערפנא אלחצ'רה מא נחן פיה וגעלנא קצדנא</l>
                <l n="9 ">באב אללה ת'ע' ובאבהא לתפעל מענא מן אלכיר חסב אהתמאמהא</l>
                <l n="10 ">ונכותהא אלמשהורה מן בני לוי עלי[ה] אלסלאם וקרבהא מן אלסלטאן ומן</l>
                <l n="11 ">כ'ואצה אללה ת'ע' יזידהא מן פצ'לה בעזתה וגלאלה ואן יתבת פיהא ומצא</l>
                <l n="12 ">חן ושכל ונשתהי מן אלבארי ת'ע' ומן גלאלהא אלמסאעדה ושלום.</l>
                <l n="">הערות</l>
                <l n="3 ">ע"א, צד ימין 1--	וללוי אמר...: דברים לג, ח.</l>
                <l n="">ברוך שומר..:  מן ההגדה של פסח</l>
                <l n="">כי אורך ימים: משלי ג, ב.</l>
                <l n="3 ">ע"א, צד שמאל 1--על הניסיונות לזהות את הנמען, שמואל הלוי, ראו גויטיין, חברה, ב, עמ' 160, הערה 21; פרידמן, זעקת שבר , עמ' 129--130.</l>
                <l n="17 ">על ה'שנאות' באלכסנדריה ראו גויטיין, חברה, ב, עמ' 278--281.</l>
                <l n="19 ">על ה'מעריבות', ברכות קריאת שמע מפויטות לתפילת ערבית, שנאמרו בבית הכנסת הירושלמי, ראו פרידמן, מחלוקת, סימן ט [פרידמן].</l>
                <l n="26 ">צירוף של תהילים עח, ג עם מד, ב.</l>
                <l n="28 ">משלי כב, כח.</l>
                <l n="29 ">משלי יז, ט.</l>
                <l n="">שוליים ימניים</l>
                <l n="1 ">משלי כד, כא.</l>
                <l>בראסים: צורת רבים של ברסאם, סוג של מחלת ריאה קטלנית שהיתה תוקפת מדי פעם בפעם כמגפה את תושבי ערי החוף במצרים. ראו למשל גיל 575, בשוליים הימניים.</l>
                <label>ע"ב</label>
                <l n="5 ">ההשלמה על פי הצעת פרידמן, שם.</l>
                <l n="12 ">11--משלי ג, ד.</l>
            </div>
        </body>
    </text>
</TEI>
