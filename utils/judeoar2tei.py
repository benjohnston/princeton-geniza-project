#!/usr/bin/python

import sys, re

head = '<TEI xmlns="http://www.tei-c.org/ns/1.0">\n';
head += '    <teiHeader>\n';
head += '        <fileDesc>\n';
head += '            <titleStmt>\n';
head += '                <title>AIU VII A 34r</title>\n';
head += '                <editor/>\n';
head += '            </titleStmt>\n';
head += '            <publicationStmt>\n';
head += '                <distributor>\n';
head += '                    <idno type="PGP">xxxx</idno>\n';
head += '                </distributor>\n';
head += '            </publicationStmt>\n';
head += '            <sourceDesc>\n';
head += '                <msDesc>\n';
head += '                    <msIdentifier>\n';
head += '                        <repository>Library</repository>\n';
head += '                        <msName/>\n';
head += '                    </msIdentifier>\n';
head += '                    <msContents>\n';
head += '                        <p>shelfmark and description</p>\n';
head += '                    </msContents>\n';
head += '                </msDesc>\n';
head += '            </sourceDesc>\n';
head += '        </fileDesc>\n';
head += '        <profileDesc>\n';
head += '            <textClass>\n';
head += '                <keywords>\n';
head += '                    <term/>\n';
head += '                </keywords>\n';
head += '            </textClass>\n';
head += '        </profileDesc>\n';
head += '        <revisionDesc>\n';
head += '            <change when="2013-12-24" who="#BJ">TEI created</change>\n';
head += '        </revisionDesc>\n';
head += '    </teiHeader>\n';
head += '    <text xml:space="preserve">\n';
head += '        <body>\n';
head += '            <div>\n';

tail = '            </div>\n';
tail += '        </body>\n';
tail += '    </text>\n';
tail += '</TEI>\n';




if len(sys.argv) < 2:
  print "No file input"

else:
  with open(sys.argv[1]) as f:
    print head
    content = f.readlines()
    for line in content:
      line = line.replace('\n', '').replace('\r', '')
      matches = re.match(r'^([0-9]{1,2})	', line, re.M|re.I)
      line = re.sub(r'<', '&lt;', line)
      line = re.sub(r'>', '&gt;', line)
      line = line.decode('utf8')[::-1]

      line = line.replace(']','--rbrack--').replace('[','--lbrack--')
      line = line.replace('--rbrack--','[').replace('--lbrack--',']')

      line = line.replace(')','--rparen--').replace('(','--lparen--')
      line = line.replace('--rparen--','(').replace('--lparen--',')')

      line = line.encode('utf8')
      if matches:
        line = re.sub(r'^([0-9]{1,2})	', '', line)
        line = re.sub(r'([0-9]{1,2})', r'<hi rend="superscript">\1</hi>', line)    
        print "            <l n='"+matches.group(1)+"'>" + line + "</l>"
      else:
        print "            <l>" + line + "</l>"
    print tail
