<TEI xmlns="http://www.tei-c.org/ns/1.0">
    <teiHeader>
        <fileDesc>
            <titleStmt>
                <title>AIU VII A 34r</title>
                <editor/>
            </titleStmt>
            <publicationStmt>
                <distributor>
                    <idno type="PGP">9142</idno>
                </distributor>
            </publicationStmt>
            <sourceDesc>
                <msDesc>
                    <msIdentifier>
                        <repository>Library</repository>
                        <msName/>
                    </msIdentifier>
                    <msContents>
                        <p>shelfmark and description</p>
                    </msContents>
                </msDesc>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <textClass>
                <keywords>
                    <term/>
                </keywords>
            </textClass>
        </profileDesc>
        <revisionDesc>
            <change when="2013-12-24" who="#BJ">TEI created</change>
        </revisionDesc>
    </teiHeader>
    <text xml:space="preserve">
        <body>
            <div>

            <l>﻿ח99</l>
            <l>'תבוא יד ותקחני בציצת ראשי': מכתב מחורז מאת חלפון אל הר"י אבן מיגש</l>
            <l>ספרד [כנראה חודש תמוז או אב, שנת 1138]</l>
            <l/>
            <label>TS NS 216.37</label>
            <l/>
            <l>חומר הכתיבה: נייר; מידותיו: 22.5 x 12.5 ס"מ. גיליון שקופל לשני דפים. מידת הרוחב שצוינה היא של הקטע כולו. דף ב שרד בשלמות ורוחבו 14.6 ס"מ.1</l>
            <l/>
            <l>זהו שריד גדול של איגרת הכתובה בכתב ידו של חלפון ושנמענו הוא '[ר'] יוסף הלוי ביר' מאיר נ"ע', היינו הר"י אבן מיגש. אמנם אין זאת האיגרת המקורית, כפי שנלמד משני עניינים: עצם הימצאותה בגניזה והעובדה שהיא כתובה על גבי נייר ממשלתי פסול, גיליון שבו שרידים בכתב ערבי מונומנטלי, ואין סביר שחלפון היה שולח את איגרתו לרב על נייר משומש. אולם הכתב מסודר ויפה ואין בו מחיקות ותיקונים ומתברר שזה העתק נקי לאחר השלמת האיגרת. פחות מחצי רוחבו של דף א שרד. השורות הראשונות של דף ב שרדו בשלמות, אבל חלק גדול נקרע וחסר בהמשך. כן חסרות השורות האחרונות של כתב היד כולו, אולם על פי הטקסט ששרד, אני משער שמספר השורות שחסרות במלואן בסוף הדף הוא קטן בלבד.</l>
            <l>יש מקום לקוות שבין שרידי הגניזה עוד יימצא הקטע או יימצאו קטעים המשלימים פחות או יותר את כתב היד. אולם חרף מצב הישרדותו הגרוע דומה שעל פי מה שמונח לפנינו אפשר לזהות ולשחזר את רוב תוכנה של האיגרת אם לא את כולו. הוא מתחלק לדעתי לארבעה חלקים: (א) דף א, שחסר ברובו, כולל דברי שבח וברכה לרב, בציון שמו המלא, כנזכר. גם שם הכותב נכתב כאן אלא שהוא לא שרד, חוץ מן המילה 'ממני'. (ב) תיאור מעלותיה של האיגרת שקיבל חלפון מן הרב, שעצם הגעתה וקבלתה בוודאי תוארו בחלק החסר בסוף דף א, ע"ב. (ג) חששותיו של חלפון מכתיבה לרב מפני שאין חלפון מסוגל לחבר דברים ההולמים את כבודו של הרב ואת האיגרת הנפלאה שלו. (ד) בקשה מיוחדת שביקש חלפון מן הרב לאחר ההתנצלות.</l>
            <l>אין ספק שאיגרת זו נכתבה בזמן שהותו של חלפון בספרד ועל פי מחקרנו קרוב לשער שזה היה בקיץ של שנת 1138. במידה מסוימת של סבירות אפשר להניח גם את חודש הכתיבה, הוא תמוז או אב.2 אין אנו יכולים לדעת אם בזמן כתיבת איגרת זו כבר נפגשו חלפון והר"י אבן מיגש פנים אל פנים, אולם ברור שכבר התכתבו האישים הללו. המוטיבים של פיאור איגרת הרב שקיבל חלפון – המצטיינת בחכמה ובתורה וביפי לשונה ובסגנונה עם ריח הבשמים הנודף ממנה – וקצרות ידו של המשיב בכתיבת התשובה הם שגרתיים למדי, אולם אינם חסרים עניין. מלבד ביטוי אחד קטוע בארמית לקראת סוף האיגרת של חלפון, היא כתובה כולה בעברית, בפרוזה מחורזת ונמלצת מאוד. לפנינו אפוא עדות יקרה נוספת על הסתגלותו של חלפון לערכיה של תרבות ספרד ואהבת הספרות והדר הלשון שבה. על פי השבחים ששיבח חלפון את איגרתו של הר"י אבן מיגש, אפשר לומר כמעט בוודאות גמורה שאף היא נכתבה בעברית מחורזת ונמלצת מאוד. גם בכך עדות יקרה על הרב שלא זאת בלבד שהיה הגדול בתורה בספרד בזמנו אלא אף היה שותף לתרבות הכללית הספרדית.3 </l>
            <l>שני חלקי האיגרת האחרים מלאים עניין בשל הפרטים הביוגרפיים החשובים שהם מספקים על הר"י אבן מיגש ועל חלפון ועוד. ידיעותינו על קורותיו של הר"י אבן מיגש דלות מאוד והן מושתתות בעיקר על פסקה ב'ספר הקבלה' של ר' אברהם אבן דאוד, ובה הדברים הללו:</l>
            <l/>
            <l>ומגדולי תלמידיו של רב יצחק בר' יעקב היו רב יוסף בר' מאיר הלוי ור' ברוך בר רב יצחק. ורב יוסף הלוי בר' מאיר אבן מאגש4 נולד באדר ראשון שנת דתתל"ז. והיה ר' מאיר אביו תלמיד חכם ונשוא פנים. ור' יוסף הורו הוא היה הבורח ממדינת גרנאטה מלפני המלך באדיס כמו שזכרנו למעלה. והיה עובד עבודת המלך אבן עבאד. ומנערותו של רב יוסף הלוי היה מכיר בו ר' יצחק בר' ברוך שאדם גדול היה בחכמה. והיה משתדל לר' מאיר ללמדו ביום ובלילה שהיו שניהם אוהבים זה את זה כנפשותם, ר' יצחק בר' ברוך ור' מאיר הלוי אבן מיגאש. וכשנכנס רב יצחק בן אלפאסי לספרד ונתיישב באליסאנה הלך ר' יוסף הלוי אליו ממדינת אשביליא והוא כבן י"ב שנה ועמד לפניו כי"ד שנה לקרוא יומם ולילה והיה לו לרב יצחק בן נבון והגדילו בחכמה וסמכו קודם פטירתו. [...] ולאחר פטירת רב יצחק ישב רב יוסף הלוי על כסאו מחדש סיון דתתס"ג עד אייר שנת דתתק"א ל"ח שנה. ובכלם היתה תורתו אומנותו וטבעו יוצא מספרד עד מצרים ועד בבל ובכל הארצות.5</l>
            <l/>
            <l>מתברר אפוא שבניגוד לר' יוסף הלוי אבן מיגש הסב, חצרן שנאלץ לברוח מגרנאדה לסביליה במחצית הראשונה של המאה האחת עשרה,6 ר' יוסף הלוי אבן מיגש הנכד, ראש הישיבה באליֻסאנה, לא היה נגוע בשררה, ומאז נערותו הקדיש את חייו יומם ולילה לתורתו, היא אומנותו. אברהם גרוסמן קבע עניין זה כאחד ההבדלים המהותיים בין הישיבה באשכנז לישיבה בספרד. אצל יהדות ספרד לא היו החצרנים ראשי ישיבה ואלה התרכזו במלאכת הקודש שבישיבה בלבד.7</l>
            <l>והנה באיגרת זו, בשורות הקטועות שבדף א, ראש ע"ב, בדברי השבח שכתב חלפון אל הר"י אבן מיגש (הנכד), אנו קוראים כדלהלן: </l>
            <l/>
            <l n="1">[... 				תורה] וגדולה במקום אחד כל שכן</l>
            <l n="2">[... 			שזכה לש]תי שולחנות שולחן ייי ושולחן</l>
            <l n="3">[המלך/מלכים 			...]אל בימיו ויחזירהו לעבודתו</l>
            <l n="4">[... 			וישיבהו על] כנו כמשפט הראשון ויזכהו</l>
            <l n="5">[...				]כל ימִ[יו ו]יִק[י]ים בו קרא דכ̇ת</l>
            <l n="6">[...				]שלום</l>
            <l/>
            <l>הגדולה שבמאמר התלמודי 'תורה וגדולה במקום אחד' מתפרשת על ידי כל הפרשנים כרומזת על שררה. אמנם 'שתי שולחנות' עשוי לרמוז על תורה ועושר. אולם דומה שאין מקום לספק בדברים שבהמשך, ובהם חלפון מברך את הרב שהאל 'יחזירהו לעבודתו [... וישיבהו על] כנו כמשפט הראשון'. המילים האחרונות לקוחות במובהק מפתרון יוסף לחלומו של שר המשקים, לומר שפרעה יחזיר אותו למשרתו. כל עוד הגיעו אלינו דברי חלפון למקוטעין בלבד, הוודאות הגמורה בהבנתם חסרה קורטוב. ואף על פי כן המסקנה הסבירה ביותר לעת עתה מאיגרת זו היא שהר"י אבן מיגש, ראש הישיבה באליֻסאנה, היה כבר בעל שררה כלשהי בחצרנות והיא נלקחה ממנו, ושחלפון בירך אותו שישיב אותו האל למשרתו 'כמשפט הראשון'. </l>
            <l>לאור איגרתו של חלפון, עלינו להודות שבשירי התהילה שכתב ר' יהודה הלוי לכבוד הר"י אבן מיגש ישנם בתים שעשויים לרמוז גם הם לכך שלרב הייתה שררה כלשהי, ובהם: 'תורה וגדֻלה חברו / ובאנשי שם אז נדברו'.14 בין כך ובין כך, בהנחה שצדקנו בפירוש השורות הקטועות שכתב חלפון, עלינו לתהות על הסתירה בינן לבין דברי אבן דאוד הנזכרים. כבר עסקנו בסתירה אחרת לכאורה בין 'ספר הקבלה', שנכתב כעשרים שנה לאחר המעשים הללו, לבין המצוי בארכיונו של חלפון, בן הזמן.15 האם במקרה זה נשמטו לגמרי הפרטים הנרמזים מידיעתו של אבן דאוד או שמא התבלבלו אצלו במידה מסוימת קורותיהם של הר"י אבן מיגש הסב והר"י אבן מיגש הנכד? ייתכן שלא נוכל להכריע בספק זה עד שיימצא קטע המשלים את השריד המתפרסם בזה.</l>
            <l>אשר לקורותיו של חלפון, כבר עמדנו על הקרבה שהייתה בינו לבין הר"י אבן מיגש ותורתו.16 במכתב (תעודה ח30) ששלח אבן ברוך ביולי 1138 אל חלפון, היושב כנראה באליֻסאנה, נזכרה החלטתו של הסוחר המצרי להמשיך את שהותו באנדלוס, ושיערתי שהחליט כך לא משיקולים מסחריים אלא בשל חפצו בקרבתם של גדולי הרוח בספרד, ובראשם הר"י אבן מיגש ור' יהודה הלוי.17 והנה בחלקה האחרון של איגרת זו חלפון מבקש מן הרב לקבל אותו כתלמידו, ואם לא – יממש את תכניתו לשוב למצרים: </l>
            <l/>
            <l>כי יוליכיני דרך הנשר בשמים / [וי]ניחיני בתוך ארץ מצרים / </l>
            <l>או תבוא יד ותקחני בציצת ראשי / ותוליכיני עד אבוא אל מקדשי18</l>
            <l/>
            <l>בהמשך רמזים ברורים ללימוד בישיבתו של הר"י אבן מיגש, שבה 'שורות' ו'חבורות', וחלפון מצפה לקיים שם: 'ואלמד אשר לא למדתי / ואעמד בסוד [אשר לא עמדתי / וא]שקוד בין העמודים / לשמוע כלמודים'.19 החרוזים הללו דומים להפליא לדברים שכתב ר' יהודה הלוי לר' חביב אלמהדוי, ושם גם הזכיר שראה את הר"י אבן מיגש דן עם תלמידיו בשאלה ששלח אליו ר' חביב. ריה"ל אף כתב שאילו רק יכול, היה רוצה לשוב ללמוד תורה כימי נעוריו: 'ואשוב ללמוד [אשר לא למדתי] / ואעמוד בסוד אשר לא עמדתי / ואשקוד בין העמודים / לשמוע כלמודים'.20 אינני יודע אם העתק של איגרת ריה"ל היה לפני חלפון והוא העתיק ממנו את הביטויים הללו או שהם היו ביטויים שגרתיים לנסיבות הנרמזות.</l>
            <l>על פי שאר הנתונים העולים מארכיונו של חלפון על שהייתו הממושכת באליֻסאנה וקרבתו לר"י אבן מיגש, אין ספק שהרב נענה לבקשתו של חלפון. ידיעותינו על התלמידים שלמדו תורה בישיבות ספרד בכלל ואצל הר"י אבן מיגש בפרט מועטות ביותר אף הן.21 כאמור, הר"י אבן מיגש היה כבן שתים עשרה בשעה שהחל את לימודיו אצל הרי"ף בישיבה שם. חלפון היה כמשוער כבן חמישים כשנסע לשם ללמוד אצל הר"י אבן מיגש. עד כמה שידוע לי לא נמצא עד כה שום מכתב מימי הביניים אל ראש ישיבה שבו בקשה להתקבל כתלמידו. גם לעניין זה נמצא הקטע המתפרסם כאן מחדש אפוא.</l>
            <l> </l>
            <l>דף א</l>
            <l/>
            <l n="1">אשר כל [.....] יגידו שבחִיִוִ ג[...				       	]</l>
            <l n="2">מקל תפארה משו. .ל.מת .[...				     וכולו]</l>
            <l n="3">מחמדים ואין דור מלא חכמה [ללא חכמתו		       	]</l>
            <l n="4">.ו..והו ירכתי תימן ל..[...				  	]</l>
            <l n="5">משכני עליון דִרִ[...]ער. ...[...				      	]</l>
            <l n="6">הנקרא בשם א[...					      	]</l>
            <l n="7">אִשִרִ לה [...							]</l>
            <l n="8">ותהלוִתִי[ו  							]</l>
            <l n="9">ובִ... למשפטִ[							]</l>
            <l n="10">לשרתו ולבִרִ[כו 							]</l>
            <l n="11">[.....] מִגד[...							]</l>
            <l n="12">ד[...								]</l>
            <l/>
            <label>Verso</label>
            <l/>
            <l n="1">[... 				תורה] וגדולה במקום אחד כל שכן</l>
            <l n="2">[... 			שזכה לש]תי שולחנות שולחן ייי ושולחן</l>
            <l n="3">[המלך/מלכים 			...]אל בימיו ויחזירהו לעבודתו</l>
            <l n="4">[... 			וישיבהו על] כנו כמשפט הראשון ויזכהו</l>
            <l n="5">[...				]כל ימִ[יו ו]יִק[י]ים בו קרא דכ̇ת</l>
            <l n="6">[...				]שלום		ממני אני הנזהר</l>
            <l n="7">[... 	חלפון הלוי בי̇ר נתנאל הלוי ̇ז]̇ל ה[.... ו]דורש אל אל בעד</l>
            <l n="8">[... 						וי]תקיים בי מלך</l>
            <l n="9">[...					̇ר] יוסף הלוי בי̇ר מאיר ̇נ̇ע </l>
            <l n="10">[...					       ].. אהבה אשקיף אל</l>
            <l n="11">[...					           ]ו          וכאשר גבירינו</l>
            <l n="12">[...						] יכין את צעדיו</l>
            <l n="13">[...							]ל... בל</l>
            <l n="14">[...							] וִלהמנות</l>
            <l n="15">[...							]ורת[...]</l>
            <l/>
            <l/>
            <l>דף ב</l>
            <l/>
            <l n="1">שכל ותִוִרה ברה / אשר לה הרקמה / להבת / שלהבת / מהר אלהים מחוצבת /</l>
            <l n="2">כל אבן יקרה מסוכתה / ספיר גזרתה / לא תסולה בכתם אופיר /</l>
            <l n="3">בשוהם יקר וספיר / פניה מאפיליה / ועניניה שלהבתיה / אילת השחר</l>
            <l n="4">שלוחה / בכל ראשי בשמים רקוחה / אפפתני בעשן בסמִיה /</l>
            <l n="5">וכסתני באבק סמִ[י]ה / ובעב[ו]ר תשובה הבהילתני / ואומר חלילה</l>
            <l n="6">חלילה לאט לי / בלבב תמ[ה] / ירא מהשיב ומהתמהמה /  כי מה אדם</l>
            <l n="7">דל וחלך / שיבו[א אחרי המ]לך / ועד זאת ידעתי כי לא אִגע</l>
            <l n="8">ולמה זה ה[ב]ל [תהבלו חזית] איש אץ בִדְבָרָיו תִקְוָה לִ̄כְסִיל [ממנו]</l>
            <l n="9">ואף ..[				]הִ דברים מתחלקת[...		]</l>
            <l n="10">מה ..[				]להשיב על א[...			]</l>
            <l n="11">או להשיג [			] אמנם קצרה [...		]</l>
            <l n="12">ויותר [				]לה עליה [...			]</l>
            <l n="13">במלים וכִף [...							]</l>
            <l n="14">.... להִ[...							]</l>
            <l/>
            <label>Verso</label>
            <l/>
            <l n="1">אשר רשפיה / רשפי אש שלהבתיה / ועל כן אפיל תחנתי / לתת בכִף</l>
            <l n="2">זכות משוגתי / ובמה יודע איפה כי ישרו דברי לפניו / וכי מצאתי</l>
            <l n="3">חן בעיניו / הלא בהושיטו שרביט האהבה / ובפניתו בעין רצון</l>
            <l n="4">ונדבה / מה ידידות משכנותיו / ומה מאוד כלתה נפשי לחצרותיו /</l>
            <l n="5">כי יוליכיני דרך הנשר בשמים / [וי]ניחיני בתוך ארץ מצרים / או</l>
            <l n="6">תבוא יד ותקחני בציצת ראשי / ותוליכיני עד אבוא אל מקדשי /</l>
            <l n="7">לראות פני מלכי וקדושי / ויוִ[דיעני ד]רכיו ואלכה באורחותיו / ואתור</l>
            <l n="8">לי מנוחה בין שורותיו / ואתִ[...		]פִניני חבורותיו / ואלמד</l>
            <l n="9">אשר לא למדתי / ואעמד בסוד [אשר לא עמדתי / וא]שקוד בין העמודים /</l>
            <l n="10">לשמוע כלמודים / [				ואתר]פק על נשף חשקי /</l>
            <l n="11">וִאִ[שי]ב לי אור באוִ[...		      			] אמשך בחבלו /</l>
            <l n="12">[וא...]ק על דגלו / וא[		   		 ] תכונתו / ואהנה מזיו</l>
            <l n="13">[תבו]נתו / ̇כ ויתקייִ[ם			ויחזו את] האלהים ויאכלו וישתו</l>
            <l n="14">[...	]מו בינוִ.[			  		  ]והי מן שמיא</l>
            <l n="15">[...	]..[				   	         ] לששון ולשמחה</l>
            <l n="16">[...							]יִ פִיִ א.ל[...]</l>
            <l/>
            </div>
        </body>
    </text>
</TEI>
